set nu
syntax on
set encoding=utf-8


colorscheme darkblue
set cursorline
filetype plugin on
map <F12> :sp <CR>:exec("tag ".expand("<cword>"))<CR>
nnoremap <Tab> <c-w>w
nnoremap <S-Tab> <c-w>W

map <F5> :!php -f %<CR>

set ignorecase
set hlsearch
set smarttab
set smartindent
set autoindent
set expandtab
set shiftwidth=4
set tabstop=4
set tags=tags;


execute pathogen#infect()
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
autocmd vimenter * NERDTree
autocmd VimEnter * wincmd p

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_php_checkers=['php','phplint']
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

let g:AutoPairsShortcutFastWrap=''

let g:airline#extensions#tabline#enabled = 1
