#!/bin/bash
cp vimrc ~/.vimrc

mkdir -p ~/.vim/autoload ~/.vim/bundle && \
curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim

cd ~/.vim/bundle

git clone --depth=1 https://github.com/vim-syntastic/syntastic.git
git clone https://github.com/bling/vim-airline
git clone https://github.com/scrooloose/nerdtree.git
git clone https://github.com/vim-scripts/AutoComplPop
git clone git://github.com/jiangmiao/auto-pairs.git

cd ../
mkdir syntax_checkers
cd syntax_checkers
curl https://www.vim.org/scripts/download_script.php?src_id=8651 > php.tar.gz
tar -xvf php.tar.gz
rm php.tar.gz
curl https://raw.githubusercontent.com/vim-syntastic/syntastic/master/syntax_checkers/php/phplint.vim > phplint.vim
cd ~/
